Proxy pi.hole over https
------------------------

This is a simple project to show how to proxy pi.hole over https.

### Requirements

You must have docker, getent and awk installed.
The last two are almost certainly installed.

You must be able to resolve the `pi.hole` dns name.
This must be running your actual pi hole installation - this project is NOT set up to run that.

### Running

Generate a certificate pair with `bin/make-cert`.
Start the container with `bin/run`.
Navigate to [https://localhost:8443](https://localhost:8443) to view your pi.hole proxy.

### Why?

A simple nginx proxy configuration will trigger an error starting:

```
[ERROR]: Unable to parse results from queryads.php
```

This fixes that.

### Next Steps

If you wish to make your pi.hole accessible to the general internet then you MUST use a REAL authentication system.
You can expose private information about your DNS habits if you do not do so.
Since you are using https you can use basic auth - the username and password will be protected by the https transport layer.
